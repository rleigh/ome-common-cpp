/*
 * #%L
 * OME-COMMON C++ library for C++ compatibility/portability
 * %%
 * Copyright © 2006 - 2015 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <chrono>
#include <fstream>
#include <sstream>
#include <thread>

#include <ome/compat/filesystem.h>

#include <ome/test/test.h>

using ome::compat::filesystem::create_directories;
using ome::compat::filesystem::path;
using ome::compat::filesystem::relative;

TEST(Filesystem, PathConstruct)
{
  path p0{};
  path p1{"/foo"};
  path p2{"foo"};
  path p3{"../foo"};
  path p4{std::string("/bar")};
  std::string s1("/baz");
  path p5{s1};
  path p6{std::move(s1)};

  // Test the equality operators here.

  // EXPECT_EQ/NE broken with path; see https://github.com/google/googletest/issues/1614
  EXPECT_TRUE(path{} == p0);
  EXPECT_TRUE(path{"/foo"} == p1);
  EXPECT_TRUE(path{"foo"} == p2);
  EXPECT_TRUE(path{"../foo"} == p3);
  EXPECT_TRUE(path{"/bar"} == p4);
  EXPECT_TRUE(path{"/baz"} == p5);
  EXPECT_TRUE(path{"/baz"} == p6);

  EXPECT_FALSE(p0 == p1);
  EXPECT_FALSE(p2 == p3);

  EXPECT_TRUE(p0 != p1);
  EXPECT_TRUE(p2 != p3);
}

TEST(Filesystem, PathOperators)
{
  path a{"/a"};
  path b{"/b"};
  path c{"/c"};

  ASSERT_TRUE(a < b);
  ASSERT_TRUE(a <= b);
  ASSERT_FALSE(c < b);
  ASSERT_FALSE(c <= b);
  ASSERT_TRUE(c > b);
  ASSERT_TRUE(c >= b);
  ASSERT_FALSE(a > b);
  ASSERT_FALSE(a >= b);
}

TEST(Filesystem, PathCopy)
{
  path p0{"/foo"};

  path p1(p0);

  EXPECT_TRUE(path{"/foo"} == p1);
  EXPECT_TRUE(p0 == p1);
}

TEST(Filesystem, PathMove)
{
  path p0{"/foo"};
  path p1(p0);
  path p2(std::move(p0));

  EXPECT_TRUE(path{"/foo"} == p2);
  EXPECT_TRUE(p1 == p2);

  EXPECT_FALSE(p0 == p1);
  EXPECT_FALSE(p0 == p2);
  EXPECT_TRUE(p0.empty());
}

TEST(Filesystem, PathAssign)
{
  path p0{"/foo"};

  path p1 = p0;

  EXPECT_TRUE(path{"/foo"} == p1);
  EXPECT_TRUE(p0 == p1);
}

TEST(Filesystem, PathMoveAssign)
{
  path p0{"/foo"};
  path p1 = p0;
  path p2 = std::move(p0);

  EXPECT_TRUE(path{"/foo"} == p2);
  EXPECT_TRUE(p1 == p2);

  EXPECT_FALSE(p0 == p1);
  EXPECT_FALSE(p0 == p2);
  EXPECT_TRUE(p0.empty());
}

TEST(Filesystem, PathClear)
{
  path p0{"foo"};
  path p1;

  EXPECT_TRUE(p0 != p1);
  p0.clear();
  EXPECT_TRUE(p0 == p1);
}

TEST(Filesystem, PathFilename)
{
  path p0{"fn"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_TRUE(path{"fn"} == p0.filename());
  EXPECT_TRUE(path{"fn"} == p1.filename());
  EXPECT_TRUE(path{"fn.ext"} == p2.filename());
  EXPECT_TRUE(path{"fn"} == p3.filename());
}

TEST(Filesystem, PathStem)
{
  path p0{"fn"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_TRUE(path{"fn"} == p0.stem());
  EXPECT_TRUE(path{"fn"} == p1.stem());
  EXPECT_TRUE(path{"fn"} == p2.stem());
  EXPECT_TRUE(path{"fn"} == p3.stem());
}

TEST(Filesystem, PathExtension)
{
  path p0{"fn.tar"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_TRUE(path{".tar"} == p0.extension());
  EXPECT_TRUE(path{""} == p1.extension());
  EXPECT_TRUE(path{".ext"} == p2.extension());
  EXPECT_TRUE(path{""} == p3.extension());
}

TEST(Filesystem, PathReplaceExtension)
{
  path p0{"fn.txt"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_TRUE(path{"fn.cpp"} == p0.replace_extension(".cpp"));
  EXPECT_TRUE(path{"fn.cpp"} == p0.replace_extension("cpp"));
  EXPECT_TRUE(path{"/d/fn.h"} == p1.replace_extension(".h"));
  EXPECT_TRUE(path{"/d1/d2/fn"} == p2.replace_extension(""));
  EXPECT_TRUE(path{"../fn.rst"} == p3.replace_extension(".rst"));
}

TEST(Filesystem, PathAppend)
{
  path p0{"/d1/d2"};

  path p1(p0);
  p1 /= path{"d3/f.txt"};

  path p2(p0);
  p2 /= std::move(path{"d3/f2.txt"});

  path p3(p0);
  p3 /= "f3.txt";

  path p4(p0);
  p4 /= std::string("f4.txt");

  EXPECT_TRUE(path{"/d1/d2/d3/f.txt"} == p1);
  EXPECT_TRUE(path{"/d1/d2/d3/f2.txt"} == p2);
  EXPECT_TRUE(path{"/d1/d2/f3.txt"} == p3);
  EXPECT_TRUE(path{"/d1/d2/f4.txt"} == p4);

  path p5 = p0 / path{"d3/f.txt"};

  path p6 = p0 / std::move(path{"d3/f2.txt"});

  path p7 = p0 / "f3.txt";

  path p8 = p0 / std::string("f4.txt");

  EXPECT_TRUE(path{"/d1/d2/d3/f.txt"} == p5);
  EXPECT_TRUE(path{"/d1/d2/d3/f2.txt"} == p6);
  EXPECT_TRUE(path{"/d1/d2/f3.txt"} == p7);
  EXPECT_TRUE(path{"/d1/d2/f4.txt"} == p8);
}

TEST(Filesystem, PathConcatenate)
{
  path p0{"/d1/d2"};

  path p1(p0);
  p1 += path{"d3/f.txt"};

  path p2(p0);
  p2 += std::move(path{"d3/f2.txt"});

  path p3(p0);
  p3 += "f3.txt";

  path p4(p0);
  p4 += std::move(std::string("f4.txt"));

  EXPECT_TRUE(path{"/d1/d2d3/f.txt"} == p1);
  EXPECT_TRUE(path{"/d1/d2d3/f2.txt"} == p2);
  EXPECT_TRUE(path{"/d1/d2f3.txt"} == p3);
  EXPECT_TRUE(path{"/d1/d2f4.txt"} == p4);
}

TEST(Filesystem, Empty)
{
  path p0{};
  path p1{""};
  path p2{"foo"};

  ASSERT_TRUE(p0.empty());
  ASSERT_TRUE(p1.empty());
  ASSERT_FALSE(p2.empty());
}

TEST(Filesystem, Output)
{
  path p{"/foo/bar"};

  std::ostringstream os;
  os << p;

  ASSERT_TRUE(!os.str().empty());
}

TEST(Filesystem, String)
{
  ASSERT_EQ(std::string{""}, path{});
  ASSERT_EQ(std::string{""}, path{""});
  ASSERT_EQ(std::string{"foo"}, path{"foo"});
  ASSERT_EQ(std::string{"/d1/d2"}, path{"/d1/d2"});
}

TEST(Filesystem, PathParentPath)
{
  path p0{"fn"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_TRUE(path{} == p0.parent_path());
  EXPECT_TRUE(path{"/d"} == p1.parent_path());
  EXPECT_TRUE(path{"/d1/d2"} == p2.parent_path());
  EXPECT_TRUE(path{".."} == p3.parent_path());
}

TEST(Filesystem, PathHasParentPath)
{
  path p0{"fn"};
  path p1{"/d/fn"};
  path p2{"/d1/d2/fn.ext"};
  path p3{"../fn"};

  EXPECT_FALSE(p0.has_parent_path());
  EXPECT_TRUE(p1.has_parent_path());
  EXPECT_TRUE(p2.has_parent_path());
  EXPECT_TRUE(p3.has_parent_path());
}

TEST(Filesystem, CurrentPath)
{
  path p = ome::compat::filesystem::current_path();
  ASSERT_TRUE(!p.empty());
}

TEST(Filesystem, Absolute)
{
  path p0 = ".";
  path p1 = ome::compat::filesystem::absolute(p0);

  ASSERT_TRUE(p0 != p1);
}

TEST(Filesystem, Canonical)
{
  path p0 = ".";
  path p1 = ome::compat::filesystem::canonical(p0);

  ASSERT_TRUE(p0 != p1);
}

// Also tests create_directories
TEST(Filesystem, Relative)
{
  path basepath{PROJECT_BINARY_DIR "/test/ome-compat"};
  create_directories(basepath / "testdir1/lib");
  create_directories(basepath / "testdir1/include");
  create_directories(basepath / "testdir2/share");

  path a(basepath / "testdir1" / "include");
  path b(basepath / "testdir1" / "lib");
  path c(basepath / "testdir1");
  path d(basepath / "testdir2");

  EXPECT_TRUE(ome::compat::filesystem::exists(a));
  EXPECT_TRUE(ome::compat::filesystem::exists(b));
  EXPECT_TRUE(ome::compat::filesystem::exists(c));
  EXPECT_TRUE(ome::compat::filesystem::exists(d));

  EXPECT_TRUE(path("../lib")          == relative(b, a));
  EXPECT_TRUE(path("lib")             == relative(b, c));
  EXPECT_TRUE(path("../testdir1/lib") == relative(b, d));
}

TEST(Filesystem, Exists)
{
  path base{PROJECT_SOURCE_DIR "/CMakeLists.txt"};
  ASSERT_TRUE(ome::compat::filesystem::exists(base));

  path invalid{PROJECT_BINARY_DIR "/nonexistent.file"};
  ASSERT_FALSE(ome::compat::filesystem::exists(invalid));
}

TEST(Filesystem, FileSize)
{
  path f{PROJECT_SOURCE_DIR "/CMakeLists.txt"};
  ASSERT_TRUE(ome::compat::filesystem::file_size(f) > 0);
}

// Different types between implementations
TEST(Filesystem, LastWriteTime)
{
  // To avoid issues with second-granularity timestamps
  std::this_thread::sleep_for(std::chrono::seconds(2));

  path basepath{PROJECT_BINARY_DIR "/test/ome-compat"};
  create_directories(basepath);

  path timefile = basepath / "time.txt";

  std::ofstream of(timefile.string().c_str());
  of << "Content" << std::endl;
  of.close();

  [[maybe_unused]] auto filetime = ome::compat::filesystem::last_write_time(timefile);

  ome::compat::filesystem::remove(timefile);
}

TEST(Filesystem, IsDirectory)
{
  path d{PROJECT_SOURCE_DIR};
  ASSERT_TRUE(ome::compat::filesystem::is_directory(d));

  path f{PROJECT_SOURCE_DIR "/CMakeLists.txt"};
  ASSERT_FALSE(ome::compat::filesystem::is_directory(f));
}

TEST(Filesystem, Remove)
{
  path i{PROJECT_BINARY_DIR "/invalid.file"};
  ASSERT_FALSE(ome::compat::filesystem::remove(i));

  path basepath{PROJECT_BINARY_DIR "/test/ome-compat"};
  create_directories(basepath);

  path delfile = basepath / "test.txt";

  std::ofstream of(delfile.string().c_str());
  of << "Content" << std::endl;
  of.close();

  ASSERT_TRUE(ome::compat::filesystem::exists(delfile));
  ASSERT_TRUE(ome::compat::filesystem::file_size(delfile) > 0);

  ome::compat::filesystem::remove(delfile);

  ASSERT_FALSE(ome::compat::filesystem::exists(delfile));
}

TEST(FileSystem, Iterator)
{
  ome::compat::filesystem::directory_iterator d{PROJECT_SOURCE_DIR};
  ome::compat::filesystem::directory_iterator end;

  for(; d != end; ++d)
    {
      std::cout << "Path: " << d->path() << std::endl;
    }
}
