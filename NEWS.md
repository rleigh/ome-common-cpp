# 6.1.0

Unreleased.

* The project has been moved to the namespace
  [codelibre/ome/ome-common-cpp](https://gitlab.com/codelibre/ome/ome-common-cpp)
  on GitLab.  All OME and OME-related projects will be located under
  [codelibre/ome](https://gitlab.com/codelibre/ome).

## Platform support

* Ubuntu 20.04 with GCC 9.3 is now a supported platform.
* MacOS 10.15 (Catalina) is now a supported platform.
* FreeBSD 12.1 is now a supported platform.
* C++ `std::filesystem` is supported with VS2017 and VS2019.
* Microsoft Visual Studio 2015 is no longer supported.
* CentOS 8 continues to be unsupported due to a lack of support by the
  EPEL package repository for required third-party dependencies.

## Library changes

Most changes are related to further removal of Boost library and header
dependencies.

* The unit-types library wrapping Boost.Units has been removed.
  Downstream users of the ome-xml library should implement their own
  unit conversions if required.
* `std::regex` is now required; Boost.Regex is no longer used as a
  fallback.  Downstream users should replace `ome::compat::regex` usage
  with `std::regex`.
* The Boost.Format library has been replaced with
  [fmt](https://github.com/fmtlib/fmt).
* The Boost.Log library has been replaced with simple logging functions
  based upon fmt.
* Last remaining inclusions of the Boost MPL headers have been removed.
* All usage of Boost.Range has been removed.
* CMake exported configuration has been updated to work with fixed
  `find_dependency`
  ([CMake !3161](https://gitlab.kitware.com/cmake/cmake/-/merge_requests/3161)).
  This was previously an undocumented no-op and is now properly
  functional.  Users of CMake 3.14.2 and later would otherwise
  experience breakage when running `find_package(OMECommon)`.

# 6.0.0

Released on the 8th June 2019.

## Platform support

* C++14 is now the minimum required language version, with C++17 being
  used optionally when available
  ([!17](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/18)).
* Microsoft Visual Studio 2017 and 2019 are now both supported
  ([!18](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/18)).
* LLVM 6 is now supported.
* GCC 9 is now supported in C++14 mode; C++17 is currently unsupported
  since this release changed the type of
  `std::filesystem::file_time_type` returned by
  `std::filesystem::last_write_time()`, which requires non-portable
  conversion to `std::chrono::system_clock::time_point`.  This will be
  worked around in a future release, or with C++20 which will
  introduce portable time point conversion functions.

## Library changes

* Dropped deprecated headers (`<ome/compat/array.h`,
  `<ome/compat/cstdint.h`, `<ome/compat/memory.h`,
  `<ome/compat/tuple.h`) from ome-compat
  ([!22](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/22)).
* All Boost libraries have been made optional
  * Removed all Boost.IOStreams
    ([!7](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/7),
    [!33](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/33),
    [!38](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/38)).
  * Removed Boost.Endian wrapper
    ([!9](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/9)).
  * Boost.Filesystem has been made optional, replaceable with
    std::filesystem or Qt5 with the CMake `filesystem` option
    ([!21](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/21),
    [!23](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/23),
    [!33](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/33),
    [!38](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/38),
    [!42](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/42)).
  * Boost.Regex has been made optional
    ([!36](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/36)).
  * Some header-only Boost libraries are still required, but will be
    made optional and/or removed entirely in future releases.
* Removed unused string trimming functions
  ([!8](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/8))
* Units support has been moved into a separate header-only library,
  `ome-unit-types` ([!11](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/11)).
* XML support has been moved into a separate library,
  `ome-xerces-util`
  ([!13](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/13),
  [!14](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/14),
  [!32](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/32)).
  This library is optional, depending upon the CMake `xmldom` option.
  Qt5Xml may be used as an alternative.
* XSLT support has been moved into a separate library,
  `ome-xalan-util`
  ([!12](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/12),
  [!26](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/26)).
  This library is optional, depending upon the CMake `xmldom` option.
  Qt5XmlPatterns may be used as an alternative.
* MPark.Variant was updated to the latest version
  ([!39](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/39)).
* Updated CMake exported library configuration to handle optional
  Boost and Qt5 exports
  ([!15](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/15),
  [!16](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/16),
  [!28](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/28),
  [!29](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/29),
  [!37](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/37)).

## Infrastructure changes

* Continuous integration testing is performed on GitLab, testing on
  FreeBSD, Linux, MacOS X and Windows platforms.
  ([!1](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/1),
  [!2](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/2),
  [!3](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/3),
  [!4](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/4),
  [!5](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/5),
  [!6](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/6),
  [!24](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/24),
  [!25](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/25),
  [!27](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/27),
  [!33](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/33),
  [!40](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/40),
  [!43](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/43)).
* Documentation link updates for GitLab migration
  ([!21](https://gitlab.com/codelibre/ome-common-cpp/merge_requests/17)).

# 5.5.0

Released on the 28th November 2017.

* Use CMP0067 to enable standard setting in feature tests
  ([#47](https://github.com/ome/ome-common-cpp/pull/47))
* CMake 3.4 is the minimum version
  ([#48](https://github.com/ome/ome-common-cpp/pull/48))
* Use C++11 `<thread>` and `<mutex>`
  ([#46](https://github.com/ome/ome-common-cpp/pull/46))
* Restore support for Boost 1.53
  ([#49](https://github.com/ome/ome-common-cpp/pull/49))
* Add support for Boost 1.65 and 1.65.1
  ([#50](https://github.com/ome/ome-common-cpp/pull/50))
* Conditionally add `Boost::log` and `Boost::log_setup` to exported
  configuration ([#51](https://github.com/ome/ome-common-cpp/pull/51))
* Drop embedded copies of CMake `FindBoost`, `FindXalanC` and
  `FindXercesC` ([#53](https://github.com/ome/ome-common-cpp/pull/53))

# 5.4.2

Released on the 12th June 2017.

* Doxygen improvements
  ([#38](https://github.com/ome/ome-common-cpp/pull/38),
  [#39](https://github.com/ome/ome-common-cpp/pull/39))
* CMake: use C++ standard variables as documented
  ([#40](https://github.com/ome/ome-common-cpp/pull/40))

# 5.4.1

Released on the 23rd May 2017.

* Add documentation links ([#35](https://github.com/ome/ome-common-cpp/pull/35))
* Add support for Boost 1.64
  ([#37](https://github.com/ome/ome-common-cpp/pull/37))

# 5.4.0

Released on the 9th February 2017.

* C++11 is now the minimum required language version, with C++14 being
  used when available
  ([#29](https://github.com/ome/ome-common-cpp/pull/29))
* enabled the use of a restricted number of C++11 features, including
  enum class, nullptr, initializer lists, range-based for loops and
  type traits ([#31](https://github.com/ome/ome-common-cpp/pull/31))
* enabled the use of C++11 syntax changes including `<::` not being
  interpreted as a trigraph and `>>` being used to close nested
  templates instead of `> >`
  ([#31](https://github.com/ome/ome-common-cpp/pull/31))
* Google Test (gtest) is no longer built separately in each source
  component; the latest gtest release now allows use as a conventional
  library ([#28](https://github.com/ome/ome-common-cpp/pull/28))
* source releases are now made directly from git with `git archive`
  ([#32](https://github.com/ome/ome-common-cpp/pull/32))
* Add support for Boost 1.63
  ([#30](https://github.com/ome/ome-common-cpp/pull/30))
* Boost MPL size limits are now set with MSVC only; they are no longer
  required for other compilers with C++11
* `<ome/compat/array.h>`, `<ome/compat/cstdint.h>`,
  `<ome/compat/memory.h>` and `<ome/compat/tuple.h>` are now
  deprecated in favor of the standard headers; they wrap the standard
  headers for backward compatibility but will be removed in the next
  breaking release
  ([#31](https://github.com/ome/ome-common-cpp/pull/31))

# 5.3.2

Released on the 12th October 2016.

* Boost 1.54 is the minimum supported version
  ([#25](https://github.com/ome/ome-common-cpp/pull/25))
* Add support for Boost 1.62
  ([#26](https://github.com/ome/ome-common-cpp/pull/26))

# 5.3.1

Released on the 18th September 2016.

* Document all namespaces in API reference ([#24](https://github.com/ome/ome-common-cpp/pull/24))

# 5.3.0

Released on the 29th July 2016.

* Added Base64 conversion functions ([#16](https://github.com/ome/ome-common-cpp/pull/16))
* Corrected Xerces UTF8 to UTF-16 transcoding for empty strings
  ([#18](https://github.com/ome/ome-common-cpp/pull/18))
* Added rankine temperature conversions
  ([#17](https://github.com/ome/ome-common-cpp/pull/17))
* Added MSVC workarounds for system macros with names matching unit
  names ([#22](https://github.com/ome/ome-common-cpp/pull/22))
* Removed VS2012 workarounds
  ([#15](https://github.com/ome/ome-common-cpp/pull/15))
* Improved CMake configuration export
  ([#21](https://github.com/ome/ome-common-cpp/pull/21))
* Added BSD licence text
  ([#23](https://github.com/ome/ome-common-cpp/pull/23))

# 5.2.0

Released on the 23rd April 2016.

* Decouple ome-common-cpp component from Bio-Formats
  ([#1](https://github.com/ome/ome-common-cpp/pull/1),
  [#9](https://github.com/ome/ome-common-cpp/pull/9))
* Use external gtest
  ([#2](https://github.com/ome/ome-common-cpp/pull/2))
* Add support for Boost 1.60
  ([#4](https://github.com/ome/ome-common-cpp/pull/4))
* Add Xalan-C support
  ([#7](https://github.com/ome/ome-common-cpp/pull/7))
* Rename all uses of bioformats to ome-common
  ([#10](https://github.com/ome/ome-common-cpp/pull/10))
* Add `source-archive` script to generate a source release
  ([#12](https://github.com/ome/ome-common-cpp/pull/12),
  [#13](https://github.com/ome/ome-common-cpp/pull/13),
  [#14](https://github.com/ome/ome-common-cpp/pull/14))
