/*
 * #%L
 * OME-COMPAT C++ library for C++ compatibility/portability
 * %%
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

/**
 * @file ome/compat/filesystem.h Filesystem type substitution.
 *
 * By default, the Standard Library filesystem implementation will be
 * used, imported into the @c ome::compat::filesystem namespace.  If
 * C++17 is not supported, then the Qt5 Core library may be used as an
 * alternative implementation providing the same interface.  Please see
 * the C++17 filesystem <a
 * href="https://en.cppreference.com/w/cpp/filesystem">documentation</a>
 * for further details.
 *
 * Note that the implementation provided by this header is a subset of
 * the full @c std::filesystem functionality, needed for the OME Files
 * libraries to function.  Missing functionality will be added on
 * request as it is needed.
 *
 * It is expected that @c std::filesystem will be the default once
 * C++17 support is available on all supported platforms.  At that
 * time, this header will be retired, and the standard library @c
 * filesystem header will be used directly.
 */

#ifndef OME_COMPAT_FILESYSTEM_H
#define OME_COMPAT_FILESYSTEM_H

#include <ome/compat/config.h>

#include <chrono>

namespace ome
{
  /**
   * Standard library compatibility.
   *
   * Wrappers for missing or broken standard library functionality to
   * improve portability to older platforms.
   */
  namespace compat
  {
  }
}


#ifdef OME_HAVE_FILESYSTEM
#include <filesystem>

namespace ome
{
  namespace compat
  {

    namespace filesystem = std::filesystem;

  }
}

#elif OME_HAVE_QT5_FILESYSTEM
#include <chrono>
#include <cstring>
#include <iterator>
#include <memory>
#include <string>
#include <system_error>
#include <QtCore/QDateTime>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>

namespace ome
{
  namespace compat
  {
    namespace filesystem
    {

      class path
      {
      public:
        using value_type = char;
        using string_type = std::basic_string<value_type>;

        path() noexcept = default;

        path(const path& p):
          _p(p._p)
        {}

        path(path&& p):
          _p(std::move(p._p))
        {}

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path(String&& source):
          _p(source)
        {}

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path(const String& source):
          _p(source)
        {}

#ifdef _MSC_VER
        path(const wchar_t *source):
		  _p(QString::fromWCharArray(source).toStdString())
		{}

        path(const std::wstring& source):
		  _p(QString::fromWCharArray(source.c_str()).toStdString())
		{}
#endif

        void
        clear()
        {
          _p.clear();
        }

        path
        filename() const
        {
          QFileInfo qp(_p.c_str());
          return qp.fileName().toStdString();
        }

        path
        stem() const
        {
          QFileInfo qp(_p.c_str());
          return qp.completeBaseName().toStdString();
        }

        path
        extension() const
        {
          QFileInfo qp(_p.c_str());
          std::string suffix = qp.suffix().toStdString();
          if (!suffix.empty())
            {
              return std::string(".") + suffix;
            }
          else
            {
              return path();
            }
        }

        path&
        replace_extension(const path& replacement = path())
        {
          path newpath{parent_path()};
          if (!newpath.empty())
            {
              newpath /= stem();
            }
          else
            {
              newpath = stem();
            }
          if(!replacement.empty())
            {
              if (replacement._p[0] != '.')
                newpath += ".";
              newpath += replacement;
            }
          *this = newpath;
          return *this;
        }

        path
        parent_path() const
        {
          QFileInfo qp(_p.c_str());
          path p = qp.dir().path().toStdString();
          // Qt5 returns "." while std::filesystem returns an empty
          // path.  Ensure no behaviour discrepancy.
          if (p.string() == ".")
            p.clear();
          return p;
        }

        bool
        has_parent_path() const
        {
          return !parent_path().empty();
        }

        path&
        operator= (const path& p)
        {
          _p = p._p;
          return *this;
        }

        path&
        operator= (path&& p)
        {
          _p = std::move(p._p);
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator= (String&& source)
        {
          _p = std::move(source);
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator= (const String& source)
        {
          _p = source;
          return *this;
        }

        path&
        operator/= (const path& p)
        {
          _p += '/';
          _p += p._p;
          return *this;
        }

        path&
        operator/= (path&& p)
        {
          _p += '/';
          _p += p._p;
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator/= (String&& source)
        {
          _p += '/';
          _p += source;
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator/= (const String& source)
        {
          _p += '/';
          _p += source;
          return *this;
        }

        path&
        operator+= (const path& p)
        {
          _p += p._p;
          return *this;
        }

        path&
        operator+= (path&& p)
        {
          _p += p._p;
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator+= (String&& source)
        {
          _p += source;
          return *this;
        }

        template<class String,
                 class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
        path&
        operator+= (const String& source)
        {
          _p += source;
          return *this;
        }

        bool
        empty() const noexcept
        {
          return _p.empty();
        }

        string_type
        string() const
        {
          return _p;
        }

#ifdef _MSC_VER
        std::wstring
        wstring() const
        {
          return QString::fromUtf8(_p.c_str()).toStdWString();
        }
#endif

      private:
        string_type _p;
      };

      inline
      bool operator== (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() == rhs.string();
      }

      inline
      bool operator!= (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() != rhs.string();
      }

      inline
      bool operator< (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() < rhs.string();
      }

      inline
      bool operator<= (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() <= rhs.string();
      }

      inline
      bool operator> (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() > rhs.string();
      }

      inline
      bool operator>= (const path& lhs, const path& rhs) noexcept
      {
        return lhs.string() >= rhs.string();
      }

      inline
      path
      operator/ (const path& lhs, const path& rhs)
      {
        path p(lhs);
        p /= rhs;
        return p;
      }

      inline
      path
      operator/ (const path& lhs, path&& rhs)
      {
        path p(lhs);
        p /= rhs;
        return p;
      }

      template<class String,
               class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
      inline
      path
      operator/ (const path& lhs, String&& rhs)
      {
        path p(lhs);
        p /= rhs;
        return p;
      }

      template<class String,
               class = typename std::enable_if<std::is_convertible<String, std::string>::value>::type>
      inline
      path
      operator/ (const path& lhs, const String& rhs)
      {
        path p(lhs);
        p /= rhs;
        return p;
      }

      /**
       * Output path to output stream.
       *
       * @param os the output stream.
       * @param rhs the boolean to output.
       * @returns the output stream.
       */
      template<class charT, class traits>
      inline std::basic_ostream<charT,traits>&
      operator<< (std::basic_ostream<charT,traits>& os,
                  const path& rhs)
      {
        return os << '"' << rhs.string() << '"';
      }

      class filesystem_error : public std::system_error
      {
      public:
        filesystem_error(const std::string& what,
                         std::error_code    ec):
          system_error(ec, what)
        {
          fs_what = create_what();
        }

        filesystem_error(const std::string& what,
                         const path&        p,
                         std::error_code    ec):
          system_error(ec, what),
          p1(p)
        {
          fs_what = create_what();
        }

        filesystem_error(const std::string& what,
                         const path& p1,
                         const path& p2,
                         std::error_code ec):
          system_error(ec, what),
          p1(p1),
          p2(p2)
        {
          fs_what = create_what();
        }

        ~filesystem_error() = default;

        const path&
        path1() const noexcept
        {
          return p1;
        }

        const path&
        path2() const noexcept
        {
          return p2;
        }

        const char*
        what() const noexcept
        {
          return fs_what.c_str();
        }

      private:
        std::string
        create_what()
        {
          std::string s1 = p1.string();
          std::string s2 = p2.string();

          std::string::size_type len = 18 + std::strlen(std::system_error::what())
            + (s1.length() ? s1.length() + 3 : 0)
            + (s2.length() ? s2.length() + 3 : 0);

          std::string ret;
          ret.reserve(len);

          ret = "filesystem error: ";
          ret += std::system_error::what();
          if (!s1.empty())
            {
              ret += " [";
              ret += s1;
              ret += ']';
            }
          if (!s2.empty())
            {
              ret += " [";
              ret += s2;
              ret += ']';
            }

          return ret;
        }

        path p1;
        path p2;
        std::string fs_what;
      };

      inline
      path
      current_path()
      {
        return QDir::currentPath().toStdString();
      }

      inline
      path
      absolute(const path& p)
      {
        QFileInfo qp(p.string().c_str());
        return qp.absoluteFilePath().toStdString();
      }

      inline
      path
      canonical(const path& p)
      {
        QFileInfo qp(p.string().c_str());
        auto ret = qp.canonicalFilePath().toStdString();
        if (ret.empty())
          {
            std::error_code ec;
            ec.assign(ENOENT, std::generic_category());
            throw filesystem_error("cannot make canonical path", p, ec);
          }
        return ret;
      }

      inline
      path
      relative(const path& p,
               const path& base = current_path())
      {
        QDir qbase(base.string().c_str());
        return qbase.relativeFilePath(p.string().c_str()).toStdString();
      }

      inline
      bool
      exists(const path& p)
      {
        QFileInfo qp(p.string().c_str());
        return qp.exists();
      }

      inline
      std::uintmax_t
      file_size(const path& p)
      {
        if(!exists(p))
          {
            std::error_code ec;
            ec.assign(ENOENT, std::generic_category());
            throw filesystem_error("cannot get file size", p, ec);
          }
        QFileInfo qp(p.string().c_str());
        return static_cast<std::uintmax_t>(qp.size());
      }

      using file_time_type = std::chrono::time_point<std::chrono::system_clock>;

      inline
      file_time_type
      last_write_time(const path& p)
      {
        if(!exists(p))
          {
            std::error_code ec;
            ec.assign(ENOENT, std::generic_category());
            throw filesystem_error("cannot get file size", p, ec);
          }
        QFileInfo qp(p.string().c_str());
        auto datetime = qp.lastModified();
        auto ret = std::chrono::system_clock::from_time_t(datetime.toTime_t());
        ret += std::chrono::milliseconds(datetime.currentMSecsSinceEpoch() -
                                         (datetime.currentSecsSinceEpoch() * 1000));
        return ret;
      }

      inline
      bool
      is_directory(const path& p)
      {
        if(!exists(p))
          return false;
        QFileInfo qp(p.string().c_str());
        return qp.isDir();
      }

      inline
      bool
      create_directories(const path& p)
      {
        QDir d(p.string().c_str());
        return d.mkpath(p.string().c_str());
      }

      inline
      bool
      remove(const path& p)
      {
        QFile f(p.string().c_str());
        return f.remove();
      }

      class directory_iterator;

      class directory_entry
      {
      public:
        directory_entry() noexcept = default;

        directory_entry(const directory_entry&) = default;

        directory_entry(directory_entry&&) = default;

        explicit
        directory_entry(const path& p):
          p(p)
        {
        }

        ~directory_entry() = default;

        directory_entry& operator=(const directory_entry&) = default;
        directory_entry& operator=(directory_entry&&) = default;

        void
        assign(const path& p)
        {
          this->p = p;
        }

        const filesystem::path&
        path() const noexcept
        {
          return p;
        }

        operator const filesystem::path& () const noexcept
        {
          return p;
        }

        bool
        exists() const
        {
          return filesystem::exists(p);
        }

        bool
        is_directory() const
        {
          return filesystem::is_directory(p);
        }

        bool
        operator== (const directory_entry& rhs)
        {
          return p == rhs;
        }

        bool
        operator!= (const directory_entry& rhs)
        {
          return p != rhs;
        }

        bool
        operator< (const directory_entry& rhs)
        {
          return p < rhs;
        }

        bool
        operator<= (const directory_entry& rhs)
        {
          return p <= rhs;
        }

        bool
        operator> (const directory_entry& rhs)
        {
          return p > rhs;
        }

        bool
        operator>= (const directory_entry& rhs)
        {
          return p >= rhs;
        }

      private:
        friend class directory_iterator;

        filesystem::path p;
      };

      class directory_iterator
      {
      public:
        using value_type = directory_entry;
        using difference_type = ptrdiff_t;
        using pointer = const directory_entry*;
        using reference = const directory_entry&;
        using iterator_category = std::input_iterator_tag;

        directory_iterator() = default;

        directory_iterator(const path& p):
          entries(std::make_shared<QFileInfoList>(QDir(p.string().c_str()).entryInfoList())),
          pos(entries->begin()),
          entry()
        {
          if (pos != entries->end())
            entry = directory_entry(path(pos->filePath().toStdString()));
          else
            entry = directory_entry();
        }

        directory_iterator(const directory_iterator& rhs) = default;

        directory_iterator(directory_iterator&& rhs) = default;

        ~directory_iterator() = default;

        directory_iterator&
        operator=(const directory_iterator& rhs) = default;

        directory_iterator&
        operator=(directory_iterator&& rhs) = default;

        const directory_entry&
        operator* () const
        {
          return entry;
        }

        const directory_entry*
        operator-> () const
        {
          return &**this;
        }

        directory_iterator&
        operator++()
        {
          if (pos != entries->end())
            ++pos;
          if (pos != entries->end())
            entry = directory_entry(path(pos->filePath().toStdString()));
          else
            entry = directory_entry();
          return *this;
        }

        directory_iterator
        operator++(int)
        {
          directory_iterator d(*this);
          ++*this;
          return d;
        }

      private:
        friend bool
        operator==(const directory_iterator& lhs,
                   const directory_iterator& rhs);

        std::shared_ptr<QFileInfoList> entries;
        QFileInfoList::iterator pos;
        directory_entry entry;
      };

      inline directory_iterator
      begin(directory_iterator iter) noexcept
      {
        return iter;
      }

      inline directory_iterator
      end(directory_iterator /* iter */) noexcept
      {
        return directory_iterator();
      }

      inline bool
      operator==(const directory_iterator& lhs,
                 const directory_iterator& rhs)
      {
        if (!lhs.entries && !rhs.entries)
          return true;
        if ((!lhs.entries && rhs.pos == rhs.entries->end()) ||
            (!rhs.entries && lhs.pos == lhs.entries->end()))
          return true;
        return lhs.pos == rhs.pos;
      }

      inline bool
      operator!=(const directory_iterator& lhs,
                 const directory_iterator& rhs)
      {
        return !(lhs == rhs);
      }

    }
  }
}

#else
#error No filesystem implementation defined
#endif

#endif // OME_COMPAT_FILESYSTEM_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
