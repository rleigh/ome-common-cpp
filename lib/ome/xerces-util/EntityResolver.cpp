/*
 * #%L
 * OME-XERCES C++ library for working with Xerces C++.
 * %%
 * Copyright © 2006 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <cassert>
#include <deque>
#include <fstream>
#include <iostream>
#include <set>
#include <utility>

#include <ome/compat/filesystem.h>

#include <ome/common/log.h>

#include <ome/xerces-util/EntityResolver.h>
#include <ome/xerces-util/String.h>

#include <ome/xerces-util/Platform.h>
#include <ome/xerces-util/dom/Document.h>
#include <ome/xerces-util/dom/Element.h>
#include <ome/xerces-util/dom/NodeList.h>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>

using ome::common::LOG_TRACE;
using ome::common::LOG_DEBUG;

namespace ome
{
  namespace common
  {
    namespace xml
    {

      EntityResolver::EntityResolver():
        xercesc::XMLEntityResolver(),
        entity_path_map(),
        entity_data_map()
      {
      }

      EntityResolver::~EntityResolver()
      {
      }

      xercesc::InputSource *
      EntityResolver::resolveEntity(xercesc::XMLResourceIdentifier* resource)
      {
        xercesc::InputSource *ret = 0;

        if (resource)
          {
            switch(resource->getResourceIdentifierType())
              {
              case xercesc::XMLResourceIdentifier::SchemaGrammar:
              case xercesc::XMLResourceIdentifier::SchemaImport:
                {
                  ret = getSource(String(resource->getSchemaLocation()));
                }
                break;
              case xercesc::XMLResourceIdentifier::ExternalEntity:
                {
                  ret = getSource(String(resource->getSystemId()));
                }
                break;
              default:
                break;
              }
          }

        return ret;
      }

      xercesc::InputSource *
      EntityResolver::getSource(const std::string& resource)
      {
        xercesc::InputSource *ret = 0;

        entity_path_map_type::const_iterator i = entity_path_map.find(resource);

        if (i != entity_path_map.end())
          {
            entity_data_map_type::iterator d = entity_data_map.find(resource);

            if (d == entity_data_map.end()) // No cached data
              {
                const ome::compat::filesystem::path& file(i->second);

                if (ome::compat::filesystem::exists(file))
                  {
                    std::ifstream in(file.string().c_str());
                    if (in)
                      {
                        std::string data;
                        std::ios::pos_type pos = in.tellg();
                        in.seekg(0, std::ios::end);
                        std::ios::pos_type len = in.tellg() - pos;
                        if (len)
                          data.reserve(static_cast<std::string::size_type>(len));
                        in.seekg(0, std::ios::beg);

                        data.assign(std::istreambuf_iterator<char>(in),
                                    std::istreambuf_iterator<char>());

                        LOG_DEBUG("EntityResolver: Registering resource data {} ({})",
                                  resource, i->second);

                        std::pair<entity_data_map_type::iterator,bool> valid =
                          entity_data_map.insert(std::make_pair(resource, data));
                        if (valid.second)
                          d = valid.first;
                      }
                    else
                      {
                        std::string fs = fmt::format("Failed to load XML schema id ‘{0}’ from file ‘{1}’",
                                                     resource, file.string());
                        std::cerr << fs << '\n';
                      }
                  }
              }

            if (d != entity_data_map.end()) // Cached data
              {
                const std::string&data(d->second);

                LOG_TRACE("EntityResolver: Returning resource {} ({})",
                          resource, i->second);

                ret = new xercesc::MemBufInputSource(reinterpret_cast<const XMLByte *>(data.c_str()),
                                                     static_cast<XMLSize_t>(data.size()),
                                                     String(i->second.string()));
              }
          }

        return ret;
      }

      void
      EntityResolver::registerEntity(const std::string&                   id,
                                     const ome::compat::filesystem::path& file)
      {
        entity_path_map_type::iterator i = entity_path_map.find(id);

        if (i == entity_path_map.end())
          {
            // Insert new entry.
            entity_path_map.insert(std::make_pair(id, canonical(file)));
          }
        else
          {
            if(canonical(file) != i->second)
              {
                std::string fs = fmt::format("Mismatch registering entity id ‘{0}’: File ‘{1}’ does not match existing cached file ‘{2}’",
                                             id, i->second.string(), file.string());
                std::cerr << fs << std::endl;
                throw std::runtime_error(fs);
              }
          }
      }

      void
      EntityResolver::registerCatalog(const ome::compat::filesystem::path& catalog)
      {
        std::set<ome::compat::filesystem::path> visited;
        std::deque<ome::compat::filesystem::path> pending;

	ome::common::xml::Platform platform;

        pending.push_back(ome::compat::filesystem::canonical(catalog));

        while(!pending.empty())
          {
            ome::compat::filesystem::path current(pending.front());
            assert(!current.empty());
            pending.pop_front();

            if (visited.find(current) != visited.end())
              {
                std::string fs = fmt::format("XML catalog ‘{}’ contains a recursive reference", current.string());
                std::cerr << fs << '\n';
                continue; // This has already been processed; break loop
              }

            ome::compat::filesystem::path currentdir(current.parent_path());
            assert(!currentdir.empty());

            std::ifstream in(current.string().c_str());
            if (in)
              {
                EntityResolver r; // Does nothing.
                dom::Document doc(dom::createDocument(in, r,
                                  dom::ParseParameters(),
                                  current.string()));
                dom::Element root(doc.getDocumentElement());
                dom::NodeList nodes(root.getChildNodes());
                for (auto& node : nodes)
                  {
                    if (node.getNodeType() == xercesc::DOMNode::ELEMENT_NODE)
                      {
                        dom::Element e(node.get(), false);
                        if (e)
                          {
                            if (e.getTagName() == "uri")
                              {
                                if (e.hasAttribute("uri") && e.hasAttribute("name"))
                                  {
                                    ome::compat::filesystem::path newid(currentdir / static_cast<std::string>(e.getAttribute("uri")));

                                    LOG_DEBUG("EntityResolver: Registering {} as {}",
                                              static_cast<std::string>(e.getAttribute("name")),
                                              ome::compat::filesystem::canonical(newid).string());
                                    registerEntity(static_cast<std::string>(e.getAttribute("name")),
                                                   ome::compat::filesystem::canonical(newid));
                                  }
                              }
                            if (e.getTagName() == "nextCatalog")
                              {
                                if (e.hasAttribute("catalog"))
                                  {
                                    ome::compat::filesystem::path newcatalog(currentdir / static_cast<std::string>(e.getAttribute("catalog")));
                                    pending.push_back(ome::compat::filesystem::canonical(newcatalog));
                                  }
                              }
                          }
                      }
                  }
              }
#ifndef NDEBUG
            // Don't make failure hard in release builds; just skip.
            else
              {
                std::string fs = fmt::format("Failed to load XML catalog from file ‘{}’", catalog.string());
                throw std::runtime_error(fs);
              }
#endif

            // Mark this file visited.
            visited.insert(current);
          }
      }

    }
  }
}
