/*
 * #%L
 * OME-COMMON C++ library for C++ compatibility/portability
 * %%
 * Copyright © 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

/**
 * @file ome/common/log.h Message logging.
 *
 * This header provides simple logging facilities, using libfmt to
 * format log messages.
 */

#ifndef OME_COMMON_LOG_H
#define OME_COMMON_LOG_H

#include <iostream>
#include <fmt/ostream.h>

namespace ome
{
  namespace common
  {

    /// Log severity.
    enum class LogSeverity
      {
       SEV_TRACE,   ///< Trace
       SEV_DEBUG,   ///< DEBUG
       SEV_INFO,    ///< Information
       SEV_WARNING, ///< Warning
       SEV_ERROR,   ///< Error
       SEV_FATAL    ///< Fatal error
      };

    /**
     * Set global logging severity.
     *
     * Log messages will be filtered such that messages with a
     * priority greater or equal to the specified severity will be
     * logged; messages with a lower priority will be discarded.
     *
     * The default severity is WARNING.
     *
    * @param severity the log severity.
     */
    void
    setLogSeverity(LogSeverity severity);

    /**
     * Get global logging severity.
     *
     * @returns the log severity.
     */
    LogSeverity
    getLogSeverity();

    namespace detail
    {

      /**
       * Get prefix for log message.
       *
       * The prefix indicates the log severity.
       *
       * @param severity the log severity.
       * @returns the prefix.
       */
      inline constexpr const char *
      logPrefix(LogSeverity severity)
      {
        switch(severity)
          {
          case LogSeverity::SEV_TRACE:
            return "T:";
            break;
          case LogSeverity::SEV_DEBUG:
            return "D:";
            break;
          case LogSeverity::SEV_INFO:
            return "I: ";
            break;
          case LogSeverity::SEV_WARNING:
            return "W: ";
            break;
          case LogSeverity::SEV_ERROR:
            return "E: ";
            break;
          case LogSeverity::SEV_FATAL:
            return "F: ";
            break;
          default:
            return "U: ";
            break;
          }
      }

    }

    /**
     * Issue a formatted log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param severity the severity level.
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    log(LogSeverity severity,
        const char *format,
        const Args & ... args)
    {
      if (severity >= ome::common::getLogSeverity())
        {
          std::clog << detail::logPrefix(severity);
          fmt::print(std::clog, format, args...);
        }
    }

    /**
     * Issue a formatted TRACE log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_TRACE(const char *format,
              const Args & ... args)
    {
      log(LogSeverity::SEV_TRACE, format, args...);
    }

    /**
     * Issue a formatted DEBUG log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_DEBUG(const char *format,
              const Args & ... args)
    {
      log(LogSeverity::SEV_DEBUG, format, args...);
    }

    /**
     * Issue a formatted INFO log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_INFO(const char *format,
             const Args & ... args)
    {
      log(LogSeverity::SEV_INFO, format, args...);
    }

    /**
     * Issue a formatted WARNING log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_WARN(const char *format,
             const Args & ... args)
    {
      log(LogSeverity::SEV_WARNING, format, args...);
    }

    /**
     * Issue a formatted ERROR log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_ERROR(const char *format,
              const Args & ... args)
    {
      log(LogSeverity::SEV_ERROR, format, args...);
    }

    /**
     * Issue a formatted FATAL log message.
     *
     * The log message is formatted with libfmt.
     *
     * @param format the format string.
     * @param args the format arguments.
     */
    template <typename... Args>
    inline void
    LOG_FATAL(const char *format,
              const Args & ... args)
    {
      log(LogSeverity::SEV_FATAL, format, args...);
    }

  }
}

#endif // OME_COMMON_LOG_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
